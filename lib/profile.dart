import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ProfileWidget extends StatefulWidget {
  ProfileWidget({Key? key}) : super(key: key);

  @override
  _ProfileWidgetState createState() => _ProfileWidgetState();
}

class _ProfileWidgetState extends State<ProfileWidget> {

  final _formKey = GlobalKey<FormState>();
  String name = '';
  String gender = 'M'; // M, F
  int age = 0;
  final _nameController = TextEditingController();
  final _ageController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _loadProfile();
  }

  Future<void> _loadProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      name = prefs.getString('name') ?? '';
      _nameController.text = name;
      gender = prefs.getString('gender') ?? 'M';
      age = prefs.getInt('age') ?? 0;
      _ageController.text = '$age';
    });
  }

  Future<void> _saveProfile() async {
    final prefs = await SharedPreferences.getInstance();
    setState(() {
      prefs.setString('name', name);
      prefs.setString('gender', gender);
      prefs.setInt('age', age);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Profile'),
      ),
      body: Form(
        key: _formKey,
        child: Container(
          padding: EdgeInsets.all(16.0),
          child: ListView(
            children: [
              TextFormField(
                autofocus: true,
                controller: _nameController,
                validator: (value) {
                  if(value == null || value.isEmpty || value.length < 5){
                    return 'กรุณาใส่ข้อมูลชื่อ';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'ชื่อ-นามสกุล'),
                onChanged: (value) {
                  setState(() {
                    name = value;
                  });
                },
              ),
              TextFormField(
                controller: _ageController,
                validator: (value) {
                  var num = int.tryParse(value!);
                  if(num == null || num <= 0){
                    return 'กรุณาใส่อายุ ที่มีค่ามากกว่าหรือเท่ากับ 0';
                  }
                  return null;
                },
                autovalidateMode: AutovalidateMode.onUserInteraction,
                decoration: InputDecoration(labelText: 'อายุ'),
                onChanged: (value) {
                  setState(() {
                    age = int.tryParse(value)!;
                  });
                },
                keyboardType: TextInputType.number,
              ),
              DropdownButtonFormField(
                value: gender,
                items: [
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.male),
                        SizedBox(width: 8.0),
                        Text('ชาย')
                      ],
                    ),
                    value: 'M',
                  ),
                  DropdownMenuItem(
                    child: Row(
                      children: [
                        Icon(Icons.female),
                        SizedBox(width: 8.0),
                        Text('หญิง')
                      ],
                    ),
                    value: 'F',
                  ),
                ],
                onChanged: (String? newValue) {
                  setState(() {
                    gender = newValue!;
                  });
                },
              ),
              ElevatedButton(onPressed: () {
                if(_formKey.currentState!.validate()) {
                  _saveProfile();
                  Navigator.pop(context);
                }
              },
              child: const Text('Save')
              )
            ],
          ),
        ),
      ),
    );
  }
}